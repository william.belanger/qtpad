#!/usr/bin/env bash
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install pipx -y
pipx install --include-deps primenote
pipx ensurepath
