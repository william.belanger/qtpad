## [1.5.4] - 2024-OCTOBER-10
### Fixed
- Font-related miscalculation of minimum title width in `Resize to content` action

## [1.5.3] - 2024-JUNE-29
### Fixed
- Fix infinite recursion bug due to pathlib new subclassing behavior (Python 3.12+)

## [1.5.2] - 2024-MARCH-28
### Fixed
- Fix rendering bug for Antidote 11 (Linux only)

## [1.5.1] - 2024-MARCH-24
### Fixed
- Truncated document UID for Antidote 11 (Windows only)

## [1.5] - 2024-MARCH-22
### Added
- Added support for exporting content in `rich text` mode with formats including plain text, Markdown, and HTML.
- Introduced an 'About' page in settings
- Included actions for utilizing the Antidote dictionary and guiding tools
- Added a status page in settings to manage and view the status of installed plugins.
- Provided a one-step automated installation script for Debian-based systems.

### Changed
- The `Save as` note action has been renamed to `Export`.
- Enabled parallel processing for Antidote corrections.

### Fixed
- Implemented error handling to avoid crashes when invalid actions are performed.
- Addressed a `Zoom Out` hotkeys compatibility issue (f6008e) for Windows users.

## [1.4.1] - 2024-MARCH-04
### Added
- Remember last 'save as' directory

### Changed
- Preload core menu by default on Linux; enable compabitility for XFCE, Cinnamon, Sway and unknowns
- Assessed Druide Antidote 11 compatibility for Linux and Windows
- Updated Debian install instructions for `pipx` user-space tool

### Fixed
- Replaced Qt's getSaveFileName with QFileDialog subclass to resolve GLib-GIO-CRITICAL error flood

## [1.4] - 2024-FEBRUARY-12
### Added
- Stacked popup message bar for hotkeys, encryption and CLI inputs
- New custom CSS properties for border color, encryption and status icons, message and popup bars
- `Zoom` action support for all modes
- Added `active` CSS selectors for palettes
- Titlebar optional elide threshold
- Jump to first text occurence in search repository tool

### Changed
- Updated all color schemes (`palettes`)
- Enabled center sizegrip for toolbar style
- Save zoom level in note profile
- Disabled encryption menu for empty notes
- Minimum resize width now equals the longuest word length in title bar

### Fixed
- Auto-resize crash for externally added empty file
- Regex crash in note launcher for non-escaped patterns
- Allow dynamic CSS fonts in `rich text` mode
- Rolled state detection error in `saveGeometry`
- Message bar visibility logic
- Prevent message bar vertical truncation for long file paths
- Pixel counter initialization timing
- Password field lock screen focus
- Rendering bug for some graphical program (console mode)
- Define environment variables before script execution (console mode)
- Race condition for externally deleted note
- Image redering when combining `reset position` and `resize to content`
- Invalid width on `resize to content` of tall images
- Load root notes on `reset position`
- Removed text arbitrary clearance in `resize to content`
- Hotkey code for zoom out on Windows
- Wizard selection visibility on Windows

## [1.3] - 2022-APR-30
### Added
- Global or per-note Fernet encryption
- Intuitive image import with `paste` action (CTRL+V)
- Optimize size of notes loaded without profile
- Words counter support for `Vim` mode
- `Snap to grid` action for note
- `Apply to all` feature in `default profile` settings
- Tray icon color selection in setup wizard
- Warning before loading many notes at once

### Changed
- Faster startup and rendering
- Improved style and CSS management, added optional user override
- Improved main menu note icons for dark themes
- Reposition notes at the nearest boundary instead of screen center
- Improved `move` dialog for Linux without `qt5-styleplugins` package
- Show existing notes as disabled items in `move` dialog
- Open `move` dialog into its parent path
- Improved relative file path and stem truncation in main menu
- Handle LibreOffice Calc and MS Excel data as text
- Separate thread for archive creation
- Refactoring and Black!

### Fixed
- Included `image` mode into settings menus
- Dynamic settings side menu width
- Allow color overrides for notes context submenus
- Steps execution order in `reset positions`
- Unmaximize note window before on `roll`
- Replaced `No available notes` main menu QLabel with QAction (Zorin OS)
- Fixed note `reset` action
- Crash on folder deletion in browser menu
- Crash on text search dialog open
- Crash on zero division error in `reset positions`


## [1.2] - 2021-DEC-27
### Added
- Post-install prompt to run PrimeNote on startup (Windows)
- Send Vim server commands all at once to avoid race conditions

### Changed
- Replaced settings GUI main layout with QSplitter widget

### Removed
- Tray icon welcome message

### Fixed
- Message bar visibility
- Vim buffer reload


## [1.1-4] - 2021-DEC-16
### Added
- Support for high DPI display

### Fixed
- Added `float` to `int` conversion for Qt resize method
- Fixed PKGBUILD make dependencies

## [1.1-3] - 2021-DEC-15
### Added
- Save platform details into crash logs

### Removed
- Customizable tray icon right click action

### Fixed
- Fixed crash on move/rename for Python &lt;3.9
- Fixed new file detection in notes repository root
- Added core menu preload for XFCE and Cinnamon


## [1.1] - 2021-DEC-14
### Added
- Cross-platform symbolic links (symlinks) support
- Settings utility for symlink creation in Windows (require UAC elevation)
- Settings GUI for tray icon color modification
- Hardcoded core actions menu for empty folders
- Checkable icons for sub-notes
- Support for `xfce4-panel`
- Vim support for `resize to content` note action
- Customizable tray icon right click action

### Changed
- Rewritten menus/core module to enable on-demand menus creation
- Improved `resize to content` height calculation for lengthy text

### Removed
- Notes repository redirection through `paths.json` (replaced by symlinks support)

### Fixed
- Color picker dialog always on top of settings GUI
- Disable note actions for tray icon in settings GUI
- Ignore core action `menu` when system tray icon not ready
- Invisible console icon


## [1.0] - 2021-DEC-02
- First official release
